module.exports = {
  entry: {
    app: ['./client/js/app']
  },
  output: {
    path: './public/dist',
    filename: '[name].js',
    publicPath: 'dist/'
  },
  devServer: {
    contentBase: './public',
    port: 3000,
    inline: true
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel',
        exclude: /node_modules/
      },
      // this is weird beyond anything...
      // css and fonts loaded into js
      {
        test: /\.css$/,
        loaders: ['style', 'css']
      },
      {
        test: /\.(png|woff|woff2|eot|ttf|svg)$/,
        loader: 'url-loader?limit=100000'
      }
    ]
  },
  resolve: {
    extensions: ['', '.js', '.json']
  }
}
