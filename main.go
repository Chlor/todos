package main

import (
	"net/http"
	"github.com/gorilla/mux"
)

const STATIC string = "public"

func notFound(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, STATIC + "/index.html")
}

func main() {
	r := mux.NewRouter().StrictSlash(true)
	fs := http.FileServer(http.Dir(STATIC))
	r/*.Host("todos.com")*/.PathPrefix("/dist").Handler(fs)
	r.NotFoundHandler = http.HandlerFunc(notFound)

	http.ListenAndServe(":8080", r)
}
