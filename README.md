# Yet another todos **[WIP]**

Just a simple todo app with golang backend and react-redux frontend

*Golang project structure isn't reflected in this repository*

## Dependencies

- Nodejs v5.5.0
- Golang v1.5

## Install

```
npm i
```

## Dev-server

webpack

```
npm start
```

## Golang

Nest this project in Golang project structre {src,pkg,dist}

```
go install
go run main.go
```
