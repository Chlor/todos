import {combineReducers, createStore, applyMiddleware} from 'redux'
import thunkMiddleware from 'redux-thunk'
import * as reducers from './reducers/todos'

export default applyMiddleware(thunkMiddleware)(createStore)(combineReducers(reducers))
