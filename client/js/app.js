import 'bootstrap-css-only/css/bootstrap.min.css'
import '../css/style.css'

import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import {Provider} from 'react-redux'

import {asyncFn} from './actions/todos'

import store from './store'

store.subscribe(() => console.log(store.getState()))
store.dispatch(asyncFn())

import App from './components/App'

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>, document.querySelector('#mount-point')
)
