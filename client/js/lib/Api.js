const URI = 'http://api.todos.com:8080'

// helpers

async function toJson(response) {
  try {
    return await response.json()
  } catch (e) {
    console.error(e)
    return await response.text()
  }
}

export async function getAll() {
  return await toJson(await fetch(`${URI}/todos`))
}

class Api {
  static async getAll() {
    return await toJson(await fetch(`${URI}/todos`))
  }

  static async get(id) {
    return await toJson(await fetch(`${URI}/todos/${id}`))
  }
}

