export const keyMirror = o => Object.keys(o).reduce((p, key) => {
  p[key] = key
  return p
}, Object.create(null))

export const idGen = (() => {
  let id = 0
  return () => id++
})()
