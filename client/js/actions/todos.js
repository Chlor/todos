import {types} from '../types'
import {idGen} from '../lib/utils'
import * as Api from '../lib/Api'

export const addTodo = (name) => ({
  type: types.ADD_TODO,
  id: idGen(),
  add_ts: new Date(),
  name
})

export const toggleTodo = ({id, done}) => {
  return {
    type: types.TOGGLE_TODO,
    end_ts: done ? new Date() : null,
    id
  }
}
export const removeTodo = (id) => ({
  type: types.REMOVE_TODO, id
})

export const setFilter = (filter) => ({
  type: types.SET_FILTER,
  filter
})

//const sleep = (n) => new Promise (r => setTimeout(r, n * 1e3))

export const asyncFn = () => async function (dispatch, getState) {
  dispatch({
    type: types.ASYNC,
    todos: await Api.getAll()
  })
}
