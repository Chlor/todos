import {types, filters} from '../types'

import {idGen} from '../lib/utils'

const initialTodo = {
  id: idGen(),
  name: 'Test Todo!',
  add_ts: new Date(),
  done: false
}

export const todos = (state = [initialTodo], {type, ...action}) => {
  switch (type) {
    case types.ADD_TODO:
      return [...state, action]
    case types.TOGGLE_TODO:
      return state.map(todo => {
        let {end_ts} = action
        if (todo.id === action.id) {
          todo.done = !todo.done
          if (end_ts) {
            todo.end_ts = end_ts
          } else {
            delete todo.end_ts
          }
        }
        return todo
      })
    case types.REMOVE_TODO:
      return state.map(todo => {
        if (todo.id === action.id) {
          todo.removed = true
        }
        return todo
      })
    case types.ASYNC:
      return [...state, ...action.todos]
    default:
      return state
  }
}

export const todoFilters = (state = filters.ALL, {type, ...action}) => {
  switch (type) {
    case types.SET_FILTER:
      return action.filter
    default:
      return state
  }
}
