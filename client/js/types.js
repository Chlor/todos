import {keyMirror} from './lib/utils'
export const types = keyMirror({
  ADD_TODO: null,
  TOGGLE_TODO: null,
  REMOVE_TODO: null,
  SET_FILTER: null,
  ASYNC: null
})

export const filters = keyMirror({
  ALL: null,
  DONE: null,
  IN_PROG: null,
  REMOVED: null
})
