import React from 'react'
import {Button} from 'react-bootstrap'

const propTypes = React.PropTypes

const FilterButton = ({name, filter, current, onSelectFilter}) => ((
  <Button onClick={() => onSelectFilter(filter)}
          bsStyle={current === filter ? 'default' : 'primary'}>{name}</Button>
))

FilterButton.propTypes = {
  name: propTypes.string.isRequired,
  filter: propTypes.string.isRequired,
  current: propTypes.string.isRequired,
  onSelectFilter: propTypes.func.isRequired
}

export default FilterButton
