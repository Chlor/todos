import React, {Component} from 'react'
import Todo from './Todo'
import {ListGroup, Panel} from 'react-bootstrap'

const TodoList = ({todos, ...metods}) =>((
  // panel has to be returned here so that list group fill works properly
  <Panel header={<h3>List</h3>} bsStyle="primary" collapsible
         defaultExpanded>
    <ListGroup fill>
      {
        todos.map((t, i) => <Todo key={i} todo={t} {...metods} />)
      }
    </ListGroup>
  </Panel>
))

export default TodoList
