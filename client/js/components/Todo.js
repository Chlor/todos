import React, {Component} from 'react'
import {ListGroupItem, Row, Col, Glyphicon} from 'react-bootstrap'

export default class Todo extends Component {
  render() {
    const todo    = this.props.todo,
          removed = todo.removed,
          remove  = removed ? '' : <Glyphicon glyph="remove"
                                              onClick={() => this.onRemoveTodo()}
                                              style={{color: 'red', verticalAlign: 'middle', marginRight: '1em'}}/>
    return (
      <ListGroupItem style={{
                     opacity: removed ? '.5' : 1,
                     pointerEvents: removed ? 'none' : 'all'}
                     }>
        <Row>
          <Col sm={11}>
            <span className="todo pointer">
              {remove}
              <span onClick={e => this.onToggleTodo(e)}
                    style={{textDecoration: todo.done ? 'line-through' : 'none'}}>
              {todo.name}
              </span>

            </span>
          </Col>
        </Row>
      </ListGroupItem>
    )
  }

  onToggleTodo() {
    this.props.onToggleTodo(this.props.todo)
  }

  onRemoveTodo() {
    this.props.onRemoveTodo(this.props.todo.id)
  }
}
