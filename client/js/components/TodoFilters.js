import React from 'react'
import {ButtonGroup} from 'react-bootstrap'
import {filters} from '../types'
import FilterButton from './FilterButton'

const filterMap = {
  [filters.ALL]: 'All',
  [filters.DONE]: 'Done',
  [filters.IN_PROG]: 'In progress',
  [filters.REMOVED]: 'Removed'
}

const TodoFilters = ({todoFilters, onSelectFilter}) => ((
    <ButtonGroup>
      {
        Object.keys(filterMap).map(key =>
          <FilterButton key={key} name={filterMap[key]}
                        filter={key}
                        current={todoFilters}
                        onSelectFilter={onSelectFilter}/>
        )
      }
    </ButtonGroup>
))

export default TodoFilters
