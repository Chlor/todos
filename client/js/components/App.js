import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Grid, Row} from 'react-bootstrap'

import Header from './Header'
import Home from '../containers/Home'

class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <Grid fluid>
          <h1>Todo</h1>
          <Home/>
        </Grid>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({todos: state.todos})

export default connect(mapStateToProps)(App)


