import React from 'react'
import {Navbar, Nav} from 'react-bootstrap'

const Header = () => {
  const navbar = (
    <Navbar inverse>
      <Navbar.Header>
        <Navbar.Brand>
          <a href="#">Go-React-Todos</a>
        </Navbar.Brand>
      </Navbar.Header>
      <Nav>
      </Nav>
    </Navbar>
  )
  return (
    navbar
  )
}
export default Header
