import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import {Input, ButtonInput} from 'react-bootstrap'

export default class AddTodo extends Component {
  render() {
    return (
      <form>
        <Input type="text" label="Name" ref="input"/>
        <ButtonInput type="submit" value="Add!" bsStyle="primary"
                     wrapperClassName="text-right"
                     onClick={e => this.handleClick(e)}
        />
      </form>
    )
  }

  handleClick(e) {
    e.preventDefault()
    let input = this.refs.input
    this.props.onAddTodo(input.getValue())
    ReactDOM.findDOMNode(input).querySelector('input').value = ''
  }
}
