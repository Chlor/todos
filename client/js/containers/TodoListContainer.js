import {connect} from 'react-redux'

import {filters} from '../types'
import {toggleTodo, removeTodo} from '../actions/todos'
import TodoList from '../components/TodoList'

const filterRemoved = (todos, fn) => {
  return todos.filter(todo => {
    return todo.removed ? false : fn(todo)
  })
}

const filterTodos = ({todos, todoFilters}) => {
  switch (todoFilters) {
    case filters.ALL:
      return filterRemoved(todos, () => true)
      return todos
    case filters.DONE:
      return filterRemoved(todos, ({done}) => done)
    case filters.IN_PROG:
      return filterRemoved(todos, ({done}) => !done)
    case filters.REMOVED:
      return todos.filter(({removed}) => removed)
    default:
      return todos
  }
}

export default connect(
  state => ({todos: filterTodos(state)}),
  dispatch => ({
    onToggleTodo(id) {
      dispatch(toggleTodo(id))
    },
    onRemoveTodo(id) {
      dispatch(removeTodo(id))
    }
  })
)(TodoList)
