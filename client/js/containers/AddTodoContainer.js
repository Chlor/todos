import {connect} from 'react-redux'

import {addTodo} from '../actions/todos'
import AddTodo from '../components/AddTodo'

export default connect(
  ({todos}) => ({todos}),
  dispatch => ({
    onAddTodo(name) {
      if (name.trim().length > 0) {
        dispatch(addTodo(name))
      }
    }
  })
)(AddTodo)
