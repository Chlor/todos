import {connect} from 'react-redux'

import {setFilter} from '../actions/todos'
import TodoFilters from '../components/TodoFilters'

export default connect(
  ({todoFilters}) => ({todoFilters}),
  dispatch => ({
    onSelectFilter(filter) {
      dispatch(setFilter(filter))
    }
  })
)(TodoFilters)
