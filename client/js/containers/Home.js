import React from 'react'
import {Row, Col, Panel, ListGroup, ListGroupItem} from 'react-bootstrap'
import * as actions from '../actions/todos'

import AddTodoContainer from './AddTodoContainer'
import TodoListContainer from './TodoListContainer'
import TodoFiltersContainer from './TodoFiltersContainer'

const Home = () => ((
  <Row>
    <Col sm={6}>
      <Panel header={<h3>New Todo</h3>} bsStyle="primary" collapsible
             defaultExpanded>
        <AddTodoContainer />
      </Panel>
      <Panel header={<h3>Filter</h3>} bsStyle="primary" collapsible>
        <TodoFiltersContainer />
      </Panel>
    </Col>
    <Col sm={6}>
      <TodoListContainer />
    </Col>
  </Row>
))

export default Home
